# Optical Astronomy Denoise

## Overview
**Optical Astronomy Denoise** is a project designed to enhance the quality of optical astronomical images by applying advanced denoising techniques. The goal is to reduce noise and improve the clarity of images used in astronomical observations.

## Key Resources & References

### Quantum Image Processing
- [How to Start Experimenting with Quantum Image Processing](https://medium.com/qiskit/how-to-start-experimenting-with-quantum-image-processing-283dddcc6ba0)
- [Qiskit Textbook: Machine Learning with Qiskit and PyTorch](https://github.com/Qiskit/textbook/blob/main/notebooks/ch-applications/machine-learning-qiskit-pytorch.ipynb)
- [Qiskit Textbook: Quantum Fourier Transform](https://github.com/Qiskit/textbook/blob/main/notebooks/ch-algorithms/quantum-fourier-transform.ipynb)
- [Qiskit Circuit Knitting Toolbox](https://github.com/Qiskit/textbook/tree/main/notebooks/ch-applications#)
- [Quantum Image Processing (MIT)](https://medium.com/mit-6-s089-intro-to-quantum-computing/quantum-image-processing-visualization-a9fde8b73554)
- [Qiskit Quantum Computing Resources](https://www.ibm.com/quantum/qiskit)
- [Quantum Computing Lab - QPIX LPP Examples](https://github.com/QuantumComputingLab/qpixlpp/tree/master/examples)

### Image Processing Techniques
- [Dividing Images into Equal Parts using OpenCV (TutorialsPoint)](https://www.tutorialspoint.com/dividing-images-into-equal-parts-using-opencv-python)
- [Dividing Images into Equal Parts using OpenCV (GeeksforGeeks)](https://www.geeksforgeeks.org/dividing-images-into-equal-parts-using-opencv-in-python/)
- [StackOverflow: How to Divide an Image, Process, and Merge Back](https://stackoverflow.com/questions/69024619/how-to-divide-an-image-to-blocks-process-them-and-merge-them-back-together-in)
- [OpenCV Python Package](https://pypi.org/project/opencv-python/)
- [Astropy: CCD Reduction and Photometry Guide](https://www.astropy.org/ccd-reduction-and-photometry-guide/v/dev/notebooks/01-05-Calibration-overview.html)

### Fourier Transform for Image Denoising
- [Image Processing with Python: Fourier Transform for Digital Images](https://medium.com/swlh/image-processing-with-python-fourier-transform-for-digital-images-bc918786e375)

### Additional Quantum Computing Resources
- [IBM Quantum Serverless](https://docs.quantum.ibm.com/run/quantum-serverless)
- [Qiskit Ecosystem Documentation](https://qiskit.github.io/ecosystem/)
- [Qiskit Circuit Library](https://docs.quantum.ibm.com/build/circuit-library)
  
## Image Processing Breakdown

### Image Split Techniques
The project involves splitting images into smaller sections for denoising. This allows each part to be processed individually before recombining them into the original image.

- [Dividing Images into Blocks using OpenCV](https://www.tutorialspoint.com/dividing-images-into-equal-parts-using-opencv-python)
- [GeeksforGeeks: OpenCV Image Division](https://www.geeksforgeeks.org/dividing-images-into-equal-parts-using-opencv-in-python/)
- [Astropy NDData Utils Documentation](https://docs.astropy.org/en/stable/nddata/utils.html)

### Fast Fourier Transform (FFT) in Denoising
The Fourier Transform is a key tool for processing and reducing noise in digital images. By transforming the image data into the frequency domain, noise can be filtered out more effectively.

- [Using Fourier Transforms for Image Denoising](https://medium.com/swlh/image-processing-with-python-fourier-transform-for-digital-images-bc918786e375)

## License
This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

## Acknowledgements
Special thanks to the contributors, the astronomy community, and all individuals involved in advancing optical astronomy and quantum image processing research.

